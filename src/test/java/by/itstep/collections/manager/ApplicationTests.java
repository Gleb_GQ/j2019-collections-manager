package by.itstep.collections.manager;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.repository.CollectionItemRepository;
import by.itstep.collections.manager.repository.CollectionItemRepositoryImpl;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.CollectionRepositoryImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class ApplicationTests {

	private CollectionRepository collectionRepository = new CollectionRepositoryImpl();
	private CollectionItemRepository itemRepository = new CollectionItemRepositoryImpl();

	@BeforeEach
	void setUp(){
		itemRepository.deleteAll();
		collectionRepository.deleteAll();
	}

	@Test
	void save_collectionWithoutItems(){
		//given
		Collection collection = Collection.builder()
				.name("my-name")
				.description("my-description")
				.imageUrl("my-image")
				.title("my-title")
				.build();
		//when
		Collection saved  = collectionRepository.create(collection);

		//then
		Assertions.assertNotNull(saved.getId());
	}

	@Test
	void save_collectionWithItems(){
		//given
		Collection collection = Collection.builder()
				.name("my-name")
				.description("my-description")
				.imageUrl("my-image")
				.title("my-title")// <- !!!
				.build();
		//when
		Collection savedCollection  = collectionRepository.create(collection);


		CollectionItem i1 = CollectionItem.builder()
				.name("item-1").collection(savedCollection).build();
		CollectionItem i2 = CollectionItem.builder()
				.name("item-2").collection(savedCollection).build();

		CollectionItem savedI1 = itemRepository.create(i1);
		CollectionItem savedI2 = itemRepository.create(i2);

		List<CollectionItem> items = new ArrayList<>();
		items.add(savedI1);
		items.add(savedI2);


		//then
		Assertions.assertNotNull(savedCollection.getId());
	}


	@Test
	void findById_happyPath(){
		//given
		Collection collection = Collection.builder()
				.name("my-name")
				.description("my-description")
				.imageUrl("my-image")
				.title("my-title")// <- !!!
				.build();

		Collection savedCollection  = collectionRepository.create(collection);


		CollectionItem i1 = CollectionItem.builder()
				.name("item-1").collection(savedCollection).build();
		CollectionItem i2 = CollectionItem.builder()
				.name("item-2").collection(savedCollection).build();

		CollectionItem savedI1 = itemRepository.create(i1);
		CollectionItem savedI2 = itemRepository.create(i2);


		//when
		Collection foundCollection = collectionRepository.findById(savedCollection.getId());

		//then
		Assertions.assertNotNull(foundCollection);
		Assertions.assertNotNull(foundCollection.getId());
		Assertions.assertNotNull(foundCollection.getItems());
		Assertions.assertEquals(2, foundCollection.getItems().size());
	}


	@Test
	void testFindAll() {

		//given
		Collection collection1 = Collection.builder()
				.name("my-name1")
				.description("my-description1")
				.imageUrl("my-image1")
				.title("my-title1")
				.build();

		Collection collection2 = Collection.builder()
				.name("my-name2")
				.description("my-description2")
				.imageUrl("my-image2")
				.title("my-title2")
				.build();
		collectionRepository.create(collection1);
		collectionRepository.create(collection2);

		//when
		List<Collection> foundCollections = collectionRepository.findAll();

		//then
		Assertions.assertEquals(2, foundCollections.size());
	}

}

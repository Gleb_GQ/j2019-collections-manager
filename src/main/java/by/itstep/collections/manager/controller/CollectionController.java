package by.itstep.collections.manager.controller;


import by.itstep.collections.manager.dto.CollectionPreviewDto;
import by.itstep.collections.manager.dto.UserFullDto;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.service.CollectionService;
import by.itstep.collections.manager.service.UserService;
import org.dom4j.rule.Mode;
import org.hibernate.tuple.component.ComponentMetamodel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class CollectionController {

    @Autowired
    private CollectionService collectionService;
    @Autowired
    private UserService userService;
    /*
    * API:
    *
    * 1. get main page
    * 2. get page from collection id
    * 3. Получить страницу с формой создания коллекции
    * 4. dive opportunuty to create collection from dorm
    * */

    @RequestMapping(method = RequestMethod.GET, value =  "/index")
    public String openMainPage(Model model){
        List<CollectionPreviewDto> found = collectionService.findAll();
        List<String> strings = Arrays.asList("a", "b","c");
        model.addAttribute("all_strings", strings);
        model.addAttribute("all_collections", generateAll());
        return "index";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/profile/{id}")
    public String openProfile(@PathVariable Long id, Model model){
        UserFullDto user = userService.findById(id);
        model.addAttribute("user", user);
        return "profile";
    }

    private List<CollectionPreviewDto> generateAll(){
        List<CollectionPreviewDto> dtos = new ArrayList<>();
        dtos.add(new CollectionPreviewDto(1L, "Super coll", "Title 1", "img url", "Bob bobson", null));
        dtos.add(new CollectionPreviewDto(2L, "Best coll", "Title 2", "img url", "Boby bobs", null));
        dtos.add(new CollectionPreviewDto(3L, "Ultra coll", "Title 3", "img url", "Bobson boby", null));
        dtos.add(new CollectionPreviewDto(4L, "Bad coll", "Title 4", "img url", "Bo bo", null));
        return dtos;
    }

}

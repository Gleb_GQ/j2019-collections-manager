package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.*;
import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.mapper.TagMapper;
import by.itstep.collections.manager.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    TagMapper tagMapper;

    @Override
    public List<TagPreviewDto> findAll() {
        List<Tag> foundTags = tagRepository.findAll();
        System.out.println("TagServiceImpl -> findAll() found: " + foundTags.size() + " Tags");
        return tagMapper.mapToDtoList(foundTags);
    }

    @Override
    public TagFullDto findById(long id) {
        Tag foundTag = tagRepository.findById(id);
        System.out.println("TagServiceImpl -> findById() found Tag with id: " + id);
        return tagMapper.mapToFullDto(foundTag);
    }

    @Override
    public TagFullDto create(TagCreateDto TagCreateDto) {

        Tag TagToCreate = tagMapper.mapToEntity(TagCreateDto);
        Tag savedTag = tagRepository.create(TagToCreate);
        System.out.println("TagServiceImpl -> create() Tag created: " + savedTag);
        return tagMapper.mapToFullDto(savedTag);
    }

    @Override
    public TagFullDto update(TagUpdateDto updateDto) {

        Tag tagToUpdate = tagMapper.mapToEntity(updateDto);
        Tag existingTag = tagRepository.findById(updateDto.getId());

        tagToUpdate.setCollections(existingTag.getCollections());

        Tag updatedTag = tagRepository.update(tagToUpdate);
        System.out.println("TagServiceImpl -> update() Tag with id: " + updateDto.getId() + " is updated");
        return  tagMapper.mapToFullDto(updatedTag);
    }

    @Override
    public void deleteById(Long id) {
        tagRepository.deleteById(id);
        System.out.println("TagServiceImpl -> deleteById() Tag with id: " + id + " is deleted.");
    }
}


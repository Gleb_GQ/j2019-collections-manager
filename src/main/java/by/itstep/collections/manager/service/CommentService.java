package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.CommentCreateDto;
import by.itstep.collections.manager.dto.CommentFullDto;
import by.itstep.collections.manager.dto.CommentPreviewDto;
import by.itstep.collections.manager.dto.CommentUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {
    List<CommentPreviewDto> findAll();

    //2. Найти по айди
    CommentFullDto findById(long id);

    //1. Сохранить новый (В этом и методе айди возвращаем сохраненную коллекцию)
    CommentFullDto create (CommentCreateDto createDto);

    //4. Обновить
    CommentFullDto update(CommentUpdateDto updateDto);

    //5. Удалить по id
    void deleteById(Long id);
}

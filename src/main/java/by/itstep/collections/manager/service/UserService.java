package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.UserCreateDto;
import by.itstep.collections.manager.dto.UserFullDto;
import by.itstep.collections.manager.dto.UserPreviewDto;
import by.itstep.collections.manager.dto.UserUpdateDto;
import by.itstep.collections.manager.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    List<UserPreviewDto> findAll();

    //2. Найти по айди
    UserFullDto findById(long id);

    //1. Сохранить новый (В этом и методе айди возвращаем сохраненную коллекцию)
    UserFullDto create (UserCreateDto userCreateDto);

    //4. Обновить
    UserFullDto update(UserUpdateDto userUpdateDto);

    //5. Удалить по id
    void deleteById(Long id);
}

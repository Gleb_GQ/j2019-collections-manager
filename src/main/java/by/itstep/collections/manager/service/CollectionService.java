package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.CollectionCreateDto;
import by.itstep.collections.manager.dto.CollectionFullDto;
import by.itstep.collections.manager.dto.CollectionPreviewDto;
import by.itstep.collections.manager.dto.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.exception.MissedUpdateIdException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CollectionService {

    List<CollectionPreviewDto> findAll();

    //2. Найти по айди
    CollectionFullDto findById(long id);

    //1. Сохранить новый (В этом и методе айди возвращаем сох
    CollectionFullDto create (CollectionCreateDto createDto) throws InvalidDtoException;

    //4. Обновить
    CollectionFullDto update(CollectionUpdateDto updateDto) throws MissedUpdateIdException;

    //5. Удалить по id
    void deleteById(Long id);

}

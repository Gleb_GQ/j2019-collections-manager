package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.TagCreateDto;
import by.itstep.collections.manager.dto.TagFullDto;
import by.itstep.collections.manager.dto.TagPreviewDto;
import by.itstep.collections.manager.dto.TagUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TagService {
    List<TagPreviewDto> findAll();

    //2. Найти по айди
    TagFullDto findById(long id);

    //1. Сохранить новый (В этом и методе айди возвращаем сохраненную коллекцию)
    TagFullDto create (TagCreateDto createDto);

    //4. Обновить
    TagFullDto update(TagUpdateDto updateDto);

    //5. Удалить по id
    void deleteById(Long id);
}

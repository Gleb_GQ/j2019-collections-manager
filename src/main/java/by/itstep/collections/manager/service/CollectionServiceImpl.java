package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.CollectionCreateDto;
import by.itstep.collections.manager.dto.CollectionFullDto;
import by.itstep.collections.manager.dto.CollectionPreviewDto;
import by.itstep.collections.manager.dto.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.exception.InvalidDtoException;
import by.itstep.collections.manager.exception.MissedUpdateIdException;
import by.itstep.collections.manager.mapper.CollectionMapper;
import by.itstep.collections.manager.repository.CollectionRepository;
import by.itstep.collections.manager.repository.UserRepository;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.CookieHandler;
import java.util.List;

@Service
public class CollectionServiceImpl implements CollectionService {

    @Autowired
    private CollectionRepository collectionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private CollectionMapper mapper;

    @Override
    public List<CollectionPreviewDto> findAll() {
        List<Collection> found = collectionRepository.findAll();
        System.out.println("CollectionServiceImpl -> found " + found.size() + "collections");
        return mapper.mapToPreviewDtoList(found);
    }

    @Override
    public CollectionFullDto findById(long id) {
        Collection found = collectionRepository.findById(id);
        System.out.println("CollectionServiceImpl -> found collection:" + found);
        return mapper.mapToFullDto(found);
    }

    @Override
    public CollectionFullDto create(CollectionCreateDto createDto) throws InvalidDtoException {
        validateCreateDto(createDto);
        Collection collectionToSave = mapper.mapToEntity(createDto, userRepository.findById(createDto.getUserId()));

        Collection created = collectionRepository.create(collectionToSave);
        System.out.println("CollectionServiceImpl -> created collection:" + created);
        return mapper.mapToFullDto(created);
    }

    @Override
    public CollectionFullDto update(CollectionUpdateDto updateDto) throws MissedUpdateIdException {
        validateUpdateDto(updateDto);

        Collection collectionToUpdate = mapper.mapToEntity(updateDto);
        Collection existingCollection = collectionRepository.findById(updateDto.getId());

        collectionToUpdate.setUser(existingCollection.getUser());
        collectionToUpdate.setComments(existingCollection.getComments());
        collectionToUpdate.setItems(existingCollection.getItems());
        collectionToUpdate.setTags(existingCollection.getTags());

        Collection updated = collectionRepository.create(collectionToUpdate);
        System.out.println("CollectionServiceImpl -> updated collection:" + updated);
        return mapper.mapToFullDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        if(collectionRepository.findById(id) == null){
            //TODO exceppetion
        }
        collectionRepository.deleteById(id);
        System.out.println("CollectionServiceImpl -> collection with id " + id + " was deleted");
    }

    private void validateCreateDto(CollectionCreateDto dto) throws InvalidDtoException {
        if(dto.getUserId() == null || dto.getDescription() == null ||
            dto.getName() == null || dto.getTitle() == null){
            //TODO
            throw new InvalidDtoException("one or more fields are null");
        }
        //1. userRpository -> foundUser
        //2. foundUser -> collections
        //3. collections -> проверитт если в них dto.getName()

        if(dto.getDescription().length() < 15){
            throw new InvalidDtoException("description length min length is 15");
        }

    }

    private void validateUpdateDto(CollectionUpdateDto dto) throws MissedUpdateIdException {
        if(dto.getId() == null){
            throw new MissedUpdateIdException("id of update DTO is null");
        }
    }

}

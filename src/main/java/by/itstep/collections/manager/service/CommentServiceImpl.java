package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.*;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.mapper.CommentMapper;
import by.itstep.collections.manager.repository.CommentRepository;
import by.itstep.collections.manager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;


@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;

    @Autowired
    CommentMapper commentMapper;

    @Override
    public List<CommentPreviewDto> findAll() {
        List<Comment> foundComments = commentRepository.findAll();
        System.out.println("CommentServiceImpl -> findAll() found: " + foundComments.size() + " Comments");
        return commentMapper.mapToDtoList(foundComments);
    }

    @Override
    public CommentFullDto findById(long id) {
        Comment foundComment = commentRepository.findById(id);
        System.out.println("CommentServiceImpl -> findById() found Comment with id: " + id);
        return commentMapper.mapToFullDto(foundComment);
    }

    @Override
    public CommentFullDto create(CommentCreateDto commentCreateDto) {

        Comment CommentToCreate = commentMapper.mapToEntity(commentCreateDto, userRepository.findById(commentCreateDto.getUserId()));
        Comment savedComment = commentRepository.create(CommentToCreate);
        System.out.println("CommentServiceImpl -> create() Comment created: " + savedComment);
        return commentMapper.mapToFullDto(savedComment);
    }

    @Override
    public CommentFullDto update(CommentUpdateDto updateDto) {

        Comment commentToUpdate = commentMapper.mapToEntity(updateDto);
        Comment existingComment = commentRepository.findById(updateDto.getId());

        commentToUpdate.setUser(existingComment.getUser());
        commentToUpdate.setCollection(existingComment.getCollection());
        commentToUpdate.setCreatedAt(new Date(Calendar.getInstance().getTime().getTime()));

        Comment updatedComment = commentRepository.update(commentToUpdate);
        System.out.println("CommentServiceImpl -> update() Comment with id: " + updateDto.getId() + " is updated");
        return  commentMapper.mapToFullDto(updatedComment);
    }

    @Override
    public void deleteById(Long id) {
        commentRepository.deleteById(id);
        System.out.println("CommentServiceImpl -> deleteById() Comment with id: " + id + " is deleted.");
    }
}
package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.CollectionItemFullDto;
import by.itstep.collections.manager.dto.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.mapper.CollectionItemMapper;
import by.itstep.collections.manager.repository.CollectionItemRepository;
import by.itstep.collections.manager.repository.CollectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectionItemServiceImpl implements CollectionItemService {

    @Autowired
    private CollectionItemMapper collectionItemMapper;

    @Autowired
    private CollectionItemRepository collectionItemRepository;

    @Autowired
    private CollectionRepository collectionRepository;

    @Override
    public List<CollectionItemPreviewDto> findAll() {
        List<CollectionItem> foundItems = collectionItemRepository.findAll();
        System.out.println("CollectionItemRepository -> findAll() found: " + foundItems.size() + " items" );
        return collectionItemMapper.mapToPreviewDtoList(foundItems);
    }

    @Override
    public CollectionItemFullDto findById(long id) {
        CollectionItem foundItem = collectionItemRepository.findById(id);
        System.out.println("CollectionServiceImpl -> found collection:" + foundItem);
        return collectionItemMapper.mapToFullDto(foundItem);
    }

    @Override
    public CollectionItemFullDto create(CollectionItemCreateDto createDto) {
        CollectionItem collectionItemToSave = collectionItemMapper.mapToEntity(createDto, collectionRepository.findById(createDto.getCollectionId())) ;
        CollectionItem created = collectionItemRepository.create(collectionItemToSave);
        System.out.println("CollectionItemServiceImpl -> created collectionItem:" + created);
        return collectionItemMapper.mapToFullDto(created);
    }

    @Override
    public CollectionItemFullDto update(CollectionItemUpdateDto updateDto) {
        CollectionItem collectionItemToUpdate = collectionItemMapper.mapToEntity(updateDto);
        CollectionItem existingItem = collectionItemRepository.findById(updateDto.getId());

        collectionItemToUpdate.setCollection(existingItem.getCollection());

        CollectionItem updated = collectionItemRepository.create(collectionItemToUpdate);
        System.out.println("CollectionItemServiceImpl -> updated collectionItem:" + updated);
        return collectionItemMapper.mapToFullDto(updated);
    }

    @Override
    public void deleteById(Long id) {
        collectionItemRepository.deleteById(id);
        System.out.println("CollectionItemServiceImpl -> collectionItem with id " + id + " was deleted");
    }
}

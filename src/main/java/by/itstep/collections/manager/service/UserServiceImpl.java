package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.UserCreateDto;
import by.itstep.collections.manager.dto.UserFullDto;
import by.itstep.collections.manager.dto.UserPreviewDto;
import by.itstep.collections.manager.dto.UserUpdateDto;
import by.itstep.collections.manager.entity.Role;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.mapper.UserMapper;
import by.itstep.collections.manager.repository.UserRepository;
import by.itstep.collections.manager.repository.UserRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    UserMapper userMapper;

    @Override
    public List<UserPreviewDto> findAll() {
        List<User> foundUsers = repository.findAll();
        List<UserPreviewDto> converted = userMapper.mapToDtoList(foundUsers);
        System.out.println("UserServiceImpl -> findAll() found: " + foundUsers.size() + " users");
        return converted;
    }

    @Override
    public UserFullDto findById(long id) {
        User foundUser = repository.findById(id);
        System.out.println("UserServiceImpl -> findById() found user with id: " + id);
        return  userMapper.mapToFullDto(foundUser);
    }

    @Override
    public UserFullDto create(UserCreateDto userCreateDto) {

        User userToCreate = userMapper.mapToEntity(userCreateDto);

        userToCreate.setRole(Role.USER);

        User savedUser = repository.create(userToCreate);
        System.out.println("UserServiceImpl -> create() User created: " + savedUser);
        return userMapper.mapToFullDto(savedUser);
    }

    @Override
    public UserFullDto update(UserUpdateDto userUpdateDto) {

        User userToUpdate = userMapper.mapToEntity(userUpdateDto);
        User existingUser = repository.findById(userUpdateDto.getId());

        userToUpdate.setRole(existingUser.getRole());
        userToUpdate.setCollections(existingUser.getCollections());
        userToUpdate.setComments(existingUser.getComments());

        User updatedUser = repository.update(userToUpdate);
        System.out.println("UserServiceImpl -> update() user with id: " + userUpdateDto.getId() + " is updated");
        return  userMapper.mapToFullDto(updatedUser);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
        System.out.println("UserServiceImpl -> deleteById() user with id: " + id + " is deleted.");
    }
}

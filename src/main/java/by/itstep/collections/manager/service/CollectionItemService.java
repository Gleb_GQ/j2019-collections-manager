package by.itstep.collections.manager.service;

import by.itstep.collections.manager.dto.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.CollectionItemFullDto;
import by.itstep.collections.manager.dto.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.CollectionItemUpdateDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CollectionItemService {
    List<CollectionItemPreviewDto> findAll();

    //2. Найти по айди
    CollectionItemFullDto findById(long id);

    //1. Сохранить новый (В этом и методе айди возвращаем сохраненную коллекцию)
    CollectionItemFullDto create (CollectionItemCreateDto collectionItemCreateDto);

    //4. Обновить
    CollectionItemFullDto update(CollectionItemUpdateDto collectionItemUpdateDto);

    //5. Удалить по id
    void deleteById(Long id);
}

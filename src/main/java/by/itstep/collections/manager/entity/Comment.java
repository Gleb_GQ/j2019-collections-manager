package by.itstep.collections.manager.entity;

import lombok.*;
import org.hibernate.annotations.MetaValue;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "comment")
public class Comment {

    @Id // pk
    @GeneratedValue(strategy = GenerationType.IDENTITY) // auto-increment
    private Long id;

    @Column(name = "message", nullable = false)
    private String message;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user; // Кто оставил

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "collection_id")
    private Collection collection; // Под кем коммент

    @Column(name = "created_at", nullable = false)
    private Date createdAt;// Дата создания коммента
}

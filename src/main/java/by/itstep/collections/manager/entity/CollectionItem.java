package by.itstep.collections.manager.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "collection_item")
public class CollectionItem {

    @Id // pk
    @GeneratedValue(strategy = GenerationType.IDENTITY) // auto-increment
    private Long id;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "collection_id", nullable = false) //Название столбика который хранит id коллекции
    private Collection collection;

    @Column(name = "image_url")
    private String umageUrl;

    @Column(name = "name", nullable = false)
    private String name;
}

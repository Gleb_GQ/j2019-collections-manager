package by.itstep.collections.manager.dto;

import by.itstep.collections.manager.entity.*;
import lombok.Data;

import java.util.List;

@Data
public class CollectionFullDto {

    private Long id;
    private String name;
    private String title;
    private String description;
    private String imageUrl;
    private List<CollectionItemPreviewDto> items;
    private List<CommentPreviewDto> comments;
    private UserPreviewDto user;
    private List<TagPreviewDto> tags;

}

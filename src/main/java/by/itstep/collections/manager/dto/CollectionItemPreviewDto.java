package by.itstep.collections.manager.dto;

import lombok.Data;

@Data
public class CollectionItemPreviewDto {

    private Long id;
    private String imageUrl;
    private String name;

}

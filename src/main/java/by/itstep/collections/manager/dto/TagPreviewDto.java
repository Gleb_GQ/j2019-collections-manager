package by.itstep.collections.manager.dto;

import lombok.Data;

@Data
public class TagPreviewDto {
    private Long id;
    private String name;
}

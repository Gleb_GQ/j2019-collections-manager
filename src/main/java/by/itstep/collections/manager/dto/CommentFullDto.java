package by.itstep.collections.manager.dto;

import by.itstep.collections.manager.entity.Collection;
import lombok.Data;

import java.sql.Date;

@Data
public class CommentFullDto {

    private Long id;
    private String message;
    private UserPreviewDto user;
    private CollectionPreviewDto collection;
    private Date createdAt;
}

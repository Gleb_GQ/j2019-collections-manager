package by.itstep.collections.manager.dto;

import lombok.Data;

@Data
public class CommentCreateDto {

    private String message;
    private Long userId;

}

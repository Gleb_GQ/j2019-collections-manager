package by.itstep.collections.manager.dto;

import lombok.Data;

@Data
public class CollectionItemFullDto {

    private Long id;
    private String name;
    private String imageUrl;
    private CollectionPreviewDto collection;

}

package by.itstep.collections.manager.dto;

import lombok.Data;

@Data
public class CommentUpdateDto {
    private Long id;
    private String message;
}

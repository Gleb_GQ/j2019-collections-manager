package by.itstep.collections.manager.dto;

import lombok.Data;

@Data
public class TagUpdateDto {
    private Long id;
    private String name;
}

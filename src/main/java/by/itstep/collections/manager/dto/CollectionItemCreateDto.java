package by.itstep.collections.manager.dto;

import lombok.Data;

@Data
public class CollectionItemCreateDto {

    private String name;
    private String imageUrl;
    private Long collectionId;

}

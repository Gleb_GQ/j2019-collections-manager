package by.itstep.collections.manager.dto;

import lombok.Data;

import java.sql.Date;

@Data
public class CommentPreviewDto {

    private Long id;
    private String message;
    private UserPreviewDto user;
    private Long collectionId;
    private Date createdAt;

}

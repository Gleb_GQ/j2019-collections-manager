package by.itstep.collections.manager.dto;

import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.Role;
import lombok.Data;

import java.util.List;

@Data
public class UserFullDto {

    private Long id;
    private String name;
    private String lastName;
    private String email;
    private String password;
    private String imageUrl;
    private List<CollectionPreviewDto> collections;
    private List<CommentPreviewDto> comments;
    private Role role;

}

package by.itstep.collections.manager.dto;

import lombok.Data;

@Data
public class UserUpdateDto {

    private Long id;
    private String name;
    private String lastName;
    private String imageUrl;
    private String email;
    private String password;

}

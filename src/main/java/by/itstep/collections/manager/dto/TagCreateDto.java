package by.itstep.collections.manager.dto;

import lombok.Data;

@Data
public class TagCreateDto {
    private String name;
}

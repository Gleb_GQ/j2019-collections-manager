package by.itstep.collections.manager.dto;

import lombok.Data;

@Data
public class CollectionItemUpdateDto {

    private Long id;
    private String imageUrl;
    private String name;

}

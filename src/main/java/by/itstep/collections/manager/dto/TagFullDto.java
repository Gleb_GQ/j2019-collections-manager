package by.itstep.collections.manager.dto;

import lombok.Data;

import java.util.List;

@Data
public class TagFullDto {
    private Long id;
    private String name;
    private List<CollectionPreviewDto> collections;
}

package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CollectionRepositoryImpl implements CollectionRepository {

    @Override
    public List<Collection> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Collection> foundList = em.createNativeQuery("SELECT * FROM `collection`", Collection.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() +" collections");
        return foundList;
    }

    @Override
    public Collection findById(long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        Collection foundCollection = em.find(Collection.class, id);

        Hibernate.initialize(foundCollection.getItems());
        Hibernate.initialize(foundCollection.getComments());
        Hibernate.initialize(foundCollection.getTags());

        em.close();
        System.out.println("Found collection: " + foundCollection);
        return foundCollection;
    }

    @Override
    public Collection create(Collection collectionToCreate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Узнать про работу метода persist
        //persist можно вызывать только в рамках транзакций!!!
        em.persist(collectionToCreate);

        em.getTransaction().commit();//Сохраняем все что есть
        em.close();
        System.out.println("Collection was created. Id: " + collectionToCreate.getId());
        return collectionToCreate;
    }

    @Override
    public Collection update(Collection collectionToUpdate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Узнать про работу метода persist
        //persist можно вызывать только в рамках транзакций!!!
        em.persist(collectionToUpdate);

        em.getTransaction().commit();//Сохраняем все что есть
        em.close();
        System.out.println("Collection was updated. Id: " + collectionToUpdate.getId());
        return collectionToUpdate;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();


        Collection collectionToDelete = em.find(Collection.class, id);
        em.remove(findById(id));

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM collection").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}

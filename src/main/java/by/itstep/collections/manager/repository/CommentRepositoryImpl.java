package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CommentRepositoryImpl implements CommentRepository {

    @Override
    public List<Comment> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Comment> foundList = em.createNativeQuery("SELECT * FROM `comment`", Comment.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() +" Comments");
        return foundList;
    }

    @Override
    public Comment findById(long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        Comment foundComment = em.find(Comment.class, id);
        Hibernate.initialize(foundComment.getCollection());
        Hibernate.initialize(foundComment.getUser());
        em.close();
        System.out.println("Found Comment: " + foundComment);
        return foundComment;
    }

    @Override
    public Comment create(Comment commentToCreate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Узнать про работу метода persist
        //persist можно вызывать только в рамках транзакций!!!
        em.persist(commentToCreate);

        em.getTransaction().commit();//Сохраняем все что есть
        em.close();
        System.out.println("Comment was created. Id: " + commentToCreate.getId());
        return commentToCreate;
    }

    @Override
    public Comment update(Comment commentToUpdate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Узнать про работу метода persist
        //persist можно вызывать только в рамках транзакций!!!
        em.persist(commentToUpdate);

        em.getTransaction().commit();//Сохраняем все что есть
        em.close();
        System.out.println("Comment was updated. Id: " + commentToUpdate.getId());
        return commentToUpdate;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();


        Comment CommentToDelete = em.find(Comment.class, id);
        em.remove(findById(id));

        em.getTransaction().commit();
        em.close();
    }
}

package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Comment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository {
    //1. Найти все
    List<Comment> findAll();

    //2. Найти по айди
    Comment findById(long id);

    //1. Сохранить новый (В этом и методе айди возвращаем сохраненную коллекцию)
    Comment create (Comment commentToCreate);

    //4. Обновить
    Comment update(Comment commentToUpdate);

    //5. Удалить по id
    void deleteById(Long id);
}

package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Tag;

import java.util.List;

public interface TagRepository {
    List<Tag> findAll();

    //2. Найти по айди
    Tag findById(long id);

    //1. Сохранить новый (В этом и методе айди возвращаем сохраненную коллекцию)
    Tag create (Tag tagToCreate);

    //4. Обновить
    Tag update(Tag tagToUpdate);

    //5. Удалить по id
    void deleteById(Long id);
}

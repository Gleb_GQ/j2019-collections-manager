package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.logging.Handler;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Override
    public List<User> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<User> foundList = em.createNativeQuery("SELECT * FROM `user`", User.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() +" Users");
        return foundList;
    }

    @Override
    public User findById(long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        User foundUser = em.find(User.class, id);
        Hibernate.initialize(foundUser.getCollections());
        em.close();
        System.out.println("Found User: " + foundUser);
        return foundUser;
    }

    @Override
    public User create(User userToCreate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Узнать про работу метода persist
        //persist можно вызывать только в рамках транзакций!!!
        em.persist(userToCreate);

        em.getTransaction().commit();//Сохраняем все что есть
        em.close();
        System.out.println("User was created. Id: " + userToCreate.getId());
        return userToCreate;
    }

    @Override
    public User update(User userToUpdate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Узнать про работу метода persist
        //persist можно вызывать только в рамках транзакций!!!
        em.persist(userToUpdate);

        em.getTransaction().commit();//Сохраняем все что есть
        em.close();
        System.out.println("User was updated. Id: " + userToUpdate.getId());
        return userToUpdate;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();


        User UserToDelete = em.find(User.class, id);
        em.remove(findById(id));

        em.getTransaction().commit();
        em.close();
    }
}

package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {

    List<User> findAll();

    //2. Найти по айди
    User findById(long id);

    //1. Сохранить новый (В этом и методе айди возвращаем сохраненную коллекцию)
    User create (User userToCreate);

    //4. Обновить
    User update(User userToUpdate);

    //5. Удалить по id
    void deleteById(Long id);

}

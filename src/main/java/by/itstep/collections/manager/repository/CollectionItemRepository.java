package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.CollectionItem;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CollectionItemRepository {

    List<CollectionItem> findAll();

    CollectionItem findById(long id);

    CollectionItem create (CollectionItem collectionItemToCreate);

    CollectionItem update(CollectionItem collectionItemToUpdate);

    void deleteById(Long id);

    void deleteAll();

}

package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Tag;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class TagRepositoryImpl implements TagRepository {

    @Override
    public List<Tag> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<Tag> foundList = em.createNativeQuery("SELECT * FROM `tag`", Tag.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() +" Tags");
        return foundList;
    }

    @Override
    public Tag findById(long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        Tag foundTag = em.find(Tag.class, id);
        Hibernate.initialize(foundTag.getCollections());
        em.close();
        System.out.println("Found Tag: " + foundTag);
        return foundTag;
    }

    @Override
    public Tag create(Tag tagToCreate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Узнать про работу метода persist
        //persist можно вызывать только в рамках транзакций!!!
        em.persist(tagToCreate);

        em.getTransaction().commit();//Сохраняем все что есть
        em.close();
        System.out.println("Tag was created. Id: " + tagToCreate.getId());
        return tagToCreate;
    }

    @Override
    public Tag update(Tag tagToUpdate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Узнать про работу метода persist
        //persist можно вызывать только в рамках транзакций!!!
        em.persist(tagToUpdate);

        em.getTransaction().commit();//Сохраняем все что есть
        em.close();
        System.out.println("Tag was updated. Id: " + tagToUpdate.getId());
        return tagToUpdate;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();


        Tag TagToDelete = em.find(Tag.class, id);
        em.remove(findById(id));

        em.getTransaction().commit();
        em.close();
    }
}

package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CollectionItemRepositoryImpl implements CollectionItemRepository {

    @Override
    public List<CollectionItem> findAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();

        List<CollectionItem> foundList = em.createNativeQuery("SELECT * FROM `collectionItem`", CollectionItem.class)
                .getResultList();

        em.close();
        System.out.println("Found " + foundList.size() +" CollectionItems");
        return foundList;
    }

    @Override
    public CollectionItem findById(long id) {

        EntityManager em = EntityManagerUtils.getEntityManager();
        CollectionItem foundCollection = em.find(CollectionItem.class, id);
        Hibernate.initialize(foundCollection.getCollection());
        em.close();
        System.out.println("Found collectionItem: " + foundCollection);
        return foundCollection;
    }

    @Override
    public CollectionItem create(CollectionItem collectionItemToCreate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Узнать про работу метода persist
        //persist можно вызывать только в рамках транзакций!!!
        em.persist(collectionItemToCreate);

        em.getTransaction().commit();//Сохраняем все что есть
        em.close();
        System.out.println("CollectionItem was created. Id: " + collectionItemToCreate.getId());
        return collectionItemToCreate;
    }

    @Override
    public CollectionItem update(CollectionItem collectionItemToUpdate) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        //Узнать про работу метода persist
        //persist можно вызывать только в рамках транзакций!!!
        em.persist(collectionItemToUpdate);

        em.getTransaction().commit();//Сохраняем все что есть
        em.close();
        System.out.println("CollectionItem was updated. Id: " + collectionItemToUpdate.getId());
        return collectionItemToUpdate;
    }

    @Override
    public void deleteById(Long id) {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();


        CollectionItem collectionItemToDelete = em.find(CollectionItem.class, id);
        em.remove(findById(id));

        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAll() {
        EntityManager em = EntityManagerUtils.getEntityManager();
        em.getTransaction().begin();

        em.createNativeQuery("DELETE FROM `collection_item`").executeUpdate();

        em.getTransaction().commit();
        em.close();
    }
}

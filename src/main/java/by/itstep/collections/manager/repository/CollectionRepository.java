package by.itstep.collections.manager.repository;

import by.itstep.collections.manager.entity.Collection;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CollectionRepository {
    //1. Найти все
    List<Collection> findAll();

    //2. Найти по айди
    Collection findById(long id);

    //1. Сохранить новый (В этом и методе айди возвращаем сохраненную коллекцию)
    Collection create (Collection collectionToCreate);

    //4. Обновить
    Collection update(Collection collectionToUpdate);

    //5. Удалить по id
    void deleteById(Long id);

    void deleteAll();

}

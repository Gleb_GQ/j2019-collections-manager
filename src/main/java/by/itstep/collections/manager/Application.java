package by.itstep.collections.manager;

import by.itstep.collections.manager.dto.CollectionCreateDto;
import by.itstep.collections.manager.dto.UserCreateDto;
import by.itstep.collections.manager.dto.UserFullDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.User;
import by.itstep.collections.manager.repository.CommentRepository;
import by.itstep.collections.manager.repository.CommentRepositoryImpl;
import by.itstep.collections.manager.service.CollectionService;
import by.itstep.collections.manager.service.CollectionServiceImpl;
import by.itstep.collections.manager.service.UserService;
import by.itstep.collections.manager.service.UserServiceImpl;
import by.itstep.collections.manager.util.EntityManagerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Date;


@SpringBootApplication
public class Application {

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
		EntityManagerUtils.getEntityManager();
	}
}
package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.TagCreateDto;
import by.itstep.collections.manager.dto.TagFullDto;
import by.itstep.collections.manager.dto.TagPreviewDto;
import by.itstep.collections.manager.dto.TagUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("tagMapper")
public class TagMapper {

    @Autowired
    private CollectionMapper collectionMapper;

    public List<TagPreviewDto> mapToDtoList(List<Tag> entities) {
        List<TagPreviewDto> dtos = new ArrayList<>();
        for(Tag tag : entities){
            TagPreviewDto previewDto = new TagPreviewDto();
            previewDto.setId(tag.getId());
            previewDto.setName(tag.getName());
            dtos.add(previewDto);
        }
        return dtos;
    }

    public TagFullDto mapToFullDto(Tag entity) {
        TagFullDto fullDto = new TagFullDto();
        fullDto.setId(entity.getId());
        fullDto.setName(entity.getName());
        fullDto.setCollections(collectionMapper.mapToPreviewDtoList(entity.getCollections()));
        return fullDto;
    }

    public Tag mapToEntity(TagCreateDto createDto){
        Tag tag = Tag.builder()
                .name(createDto.getName())
                .build();
        return tag;
    }

    public Tag mapToEntity(TagUpdateDto updateDto) {
        Tag tag = Tag.builder()
                .id(updateDto.getId())
                .name(updateDto.getName())
                .build();
        return tag;
    }
}

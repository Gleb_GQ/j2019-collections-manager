package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.*;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("userMapper")
public class UserMapper {

    @Autowired
    CollectionMapper collectionMapper;
    @Autowired
    CommentMapper commentMapper;

    public List<UserPreviewDto> mapToDtoList(List<User> entities){
        List<UserPreviewDto> dtos = new ArrayList<>();
        for (User entity : entities){
            UserPreviewDto dto = new UserPreviewDto();
            dto.setId(entity.getId());
            dto.setEmail(entity.getEmail());
            dto.setName(entity.getName());
            dto.setLastName(entity.getLastName());
            dto.setRole(entity.getRole());
            dtos.add(dto);
        }
        return dtos;
    }

    public User mapToEntity(UserCreateDto createDto){
        User user = User.builder()
                .name(createDto.getName())
                .lastName(createDto.getLastName())
                .email(createDto.getEmail())
                .password(createDto.getPassword())
                .build();
        return user;
    }

    public User mapToEntity(UserUpdateDto updateDto){
        User user = User.builder()
                .id(updateDto.getId())
                .name(updateDto.getName())
                .lastName(updateDto.getName())
                .email(updateDto.getEmail())
                .password(updateDto.getPassword())
                .build();
        return user;
    }


    public UserPreviewDto mapToPreviewDto(User user) {
        UserPreviewDto previewDto = new UserPreviewDto();
        previewDto.setId(user.getId());
        previewDto.setName(user.getName());
        previewDto.setLastName(user.getLastName());
        previewDto.setEmail(user.getEmail());
        previewDto.setRole(user.getRole());
        return  previewDto;
    }

    public UserFullDto mapToFullDto(User user) {
        UserFullDto fullDto = new UserFullDto();
        fullDto.setId(user.getId());
        fullDto.setName(user.getName());
        fullDto.setLastName(user.getLastName());
        fullDto.setEmail(user.getEmail());
        fullDto.setPassword(user.getPassword());
        fullDto.setComments(commentMapper.mapToDtoList(user.getComments()));
        fullDto.setRole(user.getRole());
        fullDto.setCollections(collectionMapper.mapToPreviewDtoList(user.getCollections()));
        return fullDto;
    }
}

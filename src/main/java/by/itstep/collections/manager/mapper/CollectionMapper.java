package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.CollectionCreateDto;
import by.itstep.collections.manager.dto.CollectionFullDto;
import by.itstep.collections.manager.dto.CollectionPreviewDto;
import by.itstep.collections.manager.dto.CollectionUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("collectionMapper")
public class CollectionMapper {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private CollectionItemMapper collectionItemMapper;
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private TagMapper tagMapper;

    public CollectionPreviewDto mapToPreviewDto (Collection entity){
        CollectionPreviewDto dto = new CollectionPreviewDto();
        dto.setId(entity.getId());
        dto.setImageUrl(entity.getImageUrl());
        dto.setName(entity.getName());
        dto.setTags(tagMapper.mapToDtoList(entity.getTags()));
        dto.setTitle(entity.getTitle());
        dto.setUserName(entity.getUser().getName() + " " + entity.getUser().getLastName());
        return  dto;
    }


    public List<CollectionPreviewDto> mapToPreviewDtoList (List<Collection> entities){
        List<CollectionPreviewDto> dtos = new ArrayList<>();
        for (Collection entity : entities){
            dtos.add(mapToPreviewDto(entity));
        }
        return dtos;
    }

    public CollectionFullDto mapToFullDto(Collection collection){
        CollectionFullDto dto = new CollectionFullDto();
        dto.setId(collection.getId());
        dto.setName(collection.getName());
        dto.setDescription(collection.getDescription());
        dto.setComments(commentMapper.mapToDtoList(collection.getComments()));
        dto.setImageUrl(collection.getImageUrl());
        dto.setItems(collectionItemMapper.mapToPreviewDtoList(collection.getItems()));
        dto.setTitle(collection.getTitle());
        dto.setTags(tagMapper.mapToDtoList(collection.getTags()));
        dto.setUser(userMapper.mapToPreviewDto(collection.getUser()));
        return dto;
    }

    public Collection mapToEntity(CollectionCreateDto createDto, User user){
        Collection collection = Collection.builder()
                .description(createDto.getDescription())
                .name(createDto.getName())
                .title(createDto.getTitle())
                .imageUrl(createDto.getImageUrl())
                .user(user)
                .build();
        return collection;
    }

    public Collection mapToEntity(CollectionUpdateDto updateDto){
        Collection collection = Collection.builder()
                .id(updateDto.getId())
                .description(updateDto.getDescription())
                .name(updateDto.getName())
                .title(updateDto.getTitle())
                .imageUrl(updateDto.getImageUrl())
                .build();
        return collection;
    }


}

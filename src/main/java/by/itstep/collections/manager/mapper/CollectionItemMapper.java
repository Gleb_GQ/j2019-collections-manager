package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.CollectionItemCreateDto;
import by.itstep.collections.manager.dto.CollectionItemFullDto;
import by.itstep.collections.manager.dto.CollectionItemPreviewDto;
import by.itstep.collections.manager.dto.CollectionItemUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.CollectionItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("collectionItemMapper")
public class CollectionItemMapper {

    @Autowired
    CollectionMapper collectionMapper;
    
    public List<CollectionItemPreviewDto> mapToPreviewDtoList(List<CollectionItem> items) {
        List<CollectionItemPreviewDto> previewDtos = new ArrayList<>();
        for(CollectionItem item : items){
            CollectionItemPreviewDto dto = new CollectionItemPreviewDto();
            dto.setId(item.getId());
            dto.setName(item.getName());
            dto.setImageUrl(item.getUmageUrl());
            previewDtos.add(dto);
        }
        return  previewDtos;
    }

    public CollectionItemFullDto mapToFullDto(CollectionItem item) {
        CollectionItemFullDto fullDto = new CollectionItemFullDto();
        fullDto.setId(item.getId());
        fullDto.setName(item.getName());
        fullDto.setImageUrl(item.getUmageUrl());
        fullDto.setCollection(collectionMapper.mapToPreviewDto(item.getCollection()));
        return fullDto;
    }

    public CollectionItem mapToEntity(CollectionItemCreateDto createDto, Collection collection) {
        CollectionItem item = CollectionItem.builder()
                .name(createDto.getName())
                .umageUrl(createDto.getImageUrl())
                .collection(collection)
                .build();
        return item;
    }

    public CollectionItem mapToEntity(CollectionItemUpdateDto updateDto) {
        CollectionItem item = CollectionItem.builder()
                .id(updateDto.getId())
                .name(updateDto.getName())
                .umageUrl(updateDto.getImageUrl())
                .build();
        return item;
    }
}

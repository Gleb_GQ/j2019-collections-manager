package by.itstep.collections.manager.mapper;

import by.itstep.collections.manager.dto.CommentCreateDto;
import by.itstep.collections.manager.dto.CommentFullDto;
import by.itstep.collections.manager.dto.CommentPreviewDto;
import by.itstep.collections.manager.dto.CommentUpdateDto;
import by.itstep.collections.manager.entity.Collection;
import by.itstep.collections.manager.entity.Comment;
import by.itstep.collections.manager.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Component("commentMapper")
public class CommentMapper {

    @Autowired
    UserMapper userMapper;
    @Autowired
    CollectionMapper collectionMapper;

    public List<CommentPreviewDto> mapToDtoList(List<Comment> entities) {
        List<CommentPreviewDto> dtos = new ArrayList<>();
        for(Comment entity : entities){
            CommentPreviewDto previewDto = new CommentPreviewDto();
            previewDto.setId(entity.getId());
            previewDto.setUser(userMapper.mapToPreviewDto(entity.getUser()));
            previewDto.setCollectionId(entity.getCollection().getId());
            previewDto.setMessage(entity.getMessage());
            previewDto.setCreatedAt(entity.getCreatedAt());
            dtos.add(previewDto);
        }
        return dtos;
    }

    public CommentFullDto mapToFullDto(Comment comment) {
        CommentFullDto fullDto = new CommentFullDto();
        fullDto.setId(comment.getId());
        fullDto.setCollection(collectionMapper.mapToPreviewDto(comment.getCollection()));
        fullDto.setCreatedAt(comment.getCreatedAt());
        fullDto.setMessage(comment.getMessage());
        fullDto.setUser(userMapper.mapToPreviewDto(comment.getUser()));
        return fullDto;
    }

    public Comment mapToEntity(CommentCreateDto commentCreateDto, User user) {
        Comment comment = Comment.builder()
                .message(commentCreateDto.getMessage())
                .user(user).build();
        return comment;
    }

    public Comment mapToEntity(CommentUpdateDto commentUpdateDto) {
        Comment comment = Comment.builder()
                .id(commentUpdateDto.getId())
                .message(commentUpdateDto.getMessage())
                .build();
        return  comment;
    }
}
